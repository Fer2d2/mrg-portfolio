import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { MrgPortfolioAppComponent } from '../app/mrg-portfolio.component';

beforeEachProviders(() => [MrgPortfolioAppComponent]);

describe('App: MrgPortfolio', () => {
  it('should create the app',
      inject([MrgPortfolioAppComponent], (app: MrgPortfolioAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'mrg-portfolio works!\'',
      inject([MrgPortfolioAppComponent], (app: MrgPortfolioAppComponent) => {
    expect(app.title).toEqual('mrg-portfolio works!');
  }));
});
