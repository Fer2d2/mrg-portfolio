import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'mrg-portfolio-app',
  templateUrl: 'mrg-portfolio.component.html',
  styleUrls: ['mrg-portfolio.component.css']
})
export class MrgPortfolioAppComponent {
  title = 'mrg-portfolio works!';
}
