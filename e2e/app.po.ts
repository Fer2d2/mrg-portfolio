export class MrgPortfolioPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('mrg-portfolio-app h1')).getText();
  }
}
