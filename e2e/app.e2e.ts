import { MrgPortfolioPage } from './app.po';

describe('mrg-portfolio App', function() {
  let page: MrgPortfolioPage;

  beforeEach(() => {
    page = new MrgPortfolioPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('mrg-portfolio works!');
  });
});
